var input = "a, b, (c, d), (e, (f, g), h), 'i, j, (k, l), m', 'n, \"o, 'p', q\", r'";
var result = SplitBalanced(input, ",");
// Results: 
    ["a", 
     " b", 
     " (c, d)", 
     " (e, (f, g), h)", 
     " 'i, j, (k, l), m'", 
     " 'n \"o, 'p', q\", r'"];
